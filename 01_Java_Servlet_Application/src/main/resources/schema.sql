-- создание таблицы
create table account
(
    -- идентификатор строки - первичный ключ (идентификатор строки, по умолчанию - уникальный, генерируется базой данных
    id         bigserial primary key,
    -- имя (макс. длина - 20, не может быть пустой)
    first_name varchar(20)        not null,
    last_name  varchar(20)        not null,
    email      varchar(20) unique not null,
    -- опыт, по умолчанию 3, есть проверка на то, чтобы опыт не был меньше 3-х лет
    experience integer default 3 check (experience >= 3),
    is_active  bool    default false
);

-- изменения размера колонки
alter table account
alter column email type varchar(30);
-- добавляем новую колонку в  таблицу
alter table account
    add column phone varchar(15);
-- добавляем новое ограничение
alter table account
    add constraint check_max_experience check (experience <= 60);
-- добавление данных в таблицу
insert into account(first_name, last_name, email)
values ('Марсель', 'Сидиков', 'sidikov.marsel@gmail.com');

insert into account(first_name, last_name, email, experience)
values ('Айрат', 'Мухутдинов', 'airat@mukhutdinov.ru', 4);

insert into account(first_name, last_name, email, experience, is_active)
values ('Даниил', 'Вдовинов', 'daniil@vdovinov.com', 3, true);

insert into account(first_name, last_name, email)
values ('Максим', 'Поздеев', 'maxpozdeev@mail.ru');

-- обновление данных в  таблице
update account
set phone = '+78372824941'
where id = 1;

update account
set is_active = true
where id = 5;

insert into account(first_name, last_name, email)
values ('Тимофей', 'Папсуев', 'tima_papsuev@mail.ru');

alter table account
    add column hash_password varchar(100);