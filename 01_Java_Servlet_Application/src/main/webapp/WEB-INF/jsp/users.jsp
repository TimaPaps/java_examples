<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="ru.ptv.site.dto.AccountDto" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Papsuev Timofey
  Date: 20.06.2021
  Time: 16:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Users</title>
</head>
<body>
<h1>USERS</h1>
<table>
    <tr>
        <th>ID</th>
        <th>FIRST NAME</th>
        <th>LAST NAME</th>
        <th>EMAIL</th>
    </tr>
<%--        <%--%>
<%--            List<AccountDto> accounts = (List<AccountDto>) request.getAttribute("accounts");--%>
<%--            for (AccountDto account : accounts) {--%>
<%--        %>--%>
<%--        <tr>--%>
<%--            <td><%=account.getId()%>--%>
<%--            </td>--%>
<%--            <td><%=account.getFirstName()%>--%>
<%--            </td>--%>
<%--            <td><%=account.getLastName()%>--%>
<%--            </td>--%>
<%--            <td><%=account.getEmail()%>--%>
<%--            </td>--%>
<%--        </tr>--%>
<%--        <%}%>--%>
    <c:forEach items="${accounts}" var="account">
        <tr>
            <td>${account.id}</td>
            <td>${account.firstName}</td>
            <td>${account.lastName}</td>
            <td>${account.email}</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
