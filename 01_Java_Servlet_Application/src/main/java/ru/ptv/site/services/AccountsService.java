package ru.ptv.site.services;

import ru.ptv.site.dto.AccountDto;
import ru.ptv.site.dto.SearchAccountDto;

import java.util.List;

/**
 * 18.06.2021 - 18:59
 * 01_Java_Servlet_Application
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface AccountsService {
    List<AccountDto> getAll();
    List<SearchAccountDto> search(String query);
}
