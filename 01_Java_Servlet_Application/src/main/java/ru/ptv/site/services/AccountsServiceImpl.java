package ru.ptv.site.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ptv.site.dto.AccountDto;
import ru.ptv.site.dto.SearchAccountDto;
import ru.ptv.site.repositories.AccountsRepository;

import java.util.List;

import static ru.ptv.site.dto.SearchAccountDto.from;

/**
 * 18.06.2021 - 19:42
 * 01_Java_Servlet_Application
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Service
public class AccountsServiceImpl implements AccountsService {

    private final AccountsRepository accountsRepository;

    @Autowired
    public AccountsServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @Override
    public List<AccountDto> getAll() {
        return AccountDto.from(accountsRepository.findAll());
    }

    @Override
    public List<SearchAccountDto> search(String query) {
        return from(accountsRepository.findByFirstNameOrLastNameContains(query));
    }
}
