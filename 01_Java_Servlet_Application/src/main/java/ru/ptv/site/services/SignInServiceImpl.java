package ru.ptv.site.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.ptv.site.models.Account;
import ru.ptv.site.repositories.AccountsRepository;

import java.util.Optional;

/**
 * 21.06.2021 - 0:38
 * 01_Java_Servlet_Application
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Service
public class SignInServiceImpl implements SignInService {

    @Autowired
    private AccountsRepository accountsRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public boolean isCorrectCredentials(String email, String password) {
        Optional<Account> accountOptional = accountsRepository.findByEmail(email);

        if (accountOptional.isPresent()) {
            Account account = accountOptional.get();
            return passwordEncoder.matches(password, account.getHashPassword());
        }
        return false;

    }
}
