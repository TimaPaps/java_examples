package ru.ptv.site.services;

/**
 * 21.06.2021 - 0:36
 * 01_Java_Servlet_Application
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface SignInService {
    boolean isCorrectCredentials(String email, String password);
}
