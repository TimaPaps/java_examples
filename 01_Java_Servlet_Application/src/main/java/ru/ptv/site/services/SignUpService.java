package ru.ptv.site.services;

import ru.ptv.site.forms.SignUpForm;

/**
 * 20.06.2021 - 19:16
 * 01_Java_Servlet_Application
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface SignUpService {
    void signUp(SignUpForm form);
}
