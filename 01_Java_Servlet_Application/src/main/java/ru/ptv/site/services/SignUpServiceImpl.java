package ru.ptv.site.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.ptv.site.forms.SignUpForm;
import ru.ptv.site.models.Account;
import ru.ptv.site.repositories.AccountsRepository;

/**
 * 20.06.2021 - 19:19
 * 01_Java_Servlet_Application
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Service
public class SignUpServiceImpl implements SignUpService {

    private final AccountsRepository accountsRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public SignUpServiceImpl(AccountsRepository accountsRepository, PasswordEncoder passwordEncoder) {
        this.accountsRepository = accountsRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void signUp(SignUpForm form) {
        Account account = Account.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .email(form.getEmail())
                .hashPassword(passwordEncoder.encode(form.getPassword()))
                .build();

        accountsRepository.save(account);
    }
}
