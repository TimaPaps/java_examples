package ru.ptv.site.listeners;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.ptv.site.config.ApplicationConfig;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;

/**
 * 20.06.2021 - 17:09
 * 01_Java_Servlet_Application
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@WebListener
public class SpringContextListener implements ServletContextListener {

    private ApplicationContext springContext;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        // при запуске контекста вы получили его
        // контекст сервлетов имеет доступ ко всем сервлетам и наоборот
        ServletContext servletContext = servletContextEvent.getServletContext();
        this.springContext = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        // положил в контекст сервлетов атрибут контекст спринга
        servletContext.setAttribute("springContext", springContext);

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        ((HikariDataSource)this.springContext.getBean(DataSource.class)).close();
    }
}
