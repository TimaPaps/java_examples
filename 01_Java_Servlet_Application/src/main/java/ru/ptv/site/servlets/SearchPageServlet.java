package ru.ptv.site.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 19.06.2021 - 0:47
 * 01_Java_Servlet_Application
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@WebServlet("/admin/search_page")
public class SearchPageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.getWriter().println("<form action=\"http://localhost:8080/admin/search\">\n" +
                "\t<input type=\"text\" name=\"query\">\n" +
                "\t<br>\n" +
                "\t<input type=\"submit\" value=\"Search\">\n" +
                "</form>");
    }
}
