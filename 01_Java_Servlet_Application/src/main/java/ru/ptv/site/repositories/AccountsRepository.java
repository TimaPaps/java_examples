package ru.ptv.site.repositories;

import ru.ptv.site.models.Account;

import java.util.List;
import java.util.Optional;

/**
 * 18.06.2021 - 20:12
 * 01_Java_Servlet_Application
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface AccountsRepository extends CrudRepository<Account, Long> {
    List<Account> findByFirstNameOrLastNameContains(String name);
    Optional<Account> findByEmail(String email);
}
