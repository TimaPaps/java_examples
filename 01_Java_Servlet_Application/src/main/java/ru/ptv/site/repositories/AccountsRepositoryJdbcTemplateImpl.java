package ru.ptv.site.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.ptv.site.models.Account;

import java.util.*;

/**
 * 18.06.2021 - 20:19
 * 01_Java_Servlet_Application
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Repository
public class AccountsRepositoryJdbcTemplateImpl implements AccountsRepository {

    /*
        // for jdbcTemplate
        //language=SQL
        private static final String SQL_SEARCH = "select * from account " +
                "where first_name ilike (?) or last_name ilike (?)";
    */

    // for namedJdbcTemplate
    //language=SQL
    private static final String SQL_SEARCH = "select * from account " +
            "where first_name ilike (:query) or last_name ilike (:query)";

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from account";

    //language=SQL
    private static final String SQL_INSERT_USER = "insert into account(first_name, last_name, email, hash_password) " +
            "values (:firstName, :lastName, :email, :hashPassword)";

    //language=SQL
    private static final String SQL_FIND_BY_EMAIL = "select * from account where email = :email";



    /*
        private final JdbcTemplate jdbcTemplate;

        @Autowired
        public AccountsRepositoryJdbcTemplateImpl(JdbcTemplate jdbcTemplate) {
            this.jdbcTemplate = jdbcTemplate;
        }
    */
    private final NamedParameterJdbcTemplate namedJdbcTemplate;

    public AccountsRepositoryJdbcTemplateImpl(NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.namedJdbcTemplate = namedJdbcTemplate;
    }

    private final RowMapper<Account> accountRowMapper = (row, rowNumber) ->
            Account.builder()
                    .id(row.getLong("id"))
                    .firstName(row.getString("first_name"))
                    .lastName(row.getString("last_name"))
                    .experience(row.getInt("experience"))
                    .email(row.getString("email"))
                    .hashPassword(row.getString("hash_password"))
                    .build();

    @Override
    public List<Account> findByFirstNameOrLastNameContains(String name) {
    /*
        // for jdbcTemplate
        return jdbcTemplate.query(SQL_SEARCH, accountRowMapper, "%" + name + "%", "%" + name + "%");
    */
        // for namedJdbcTemplate
        Map<String, Object> params = Collections.singletonMap("query", "%" + name + "%");
        return namedJdbcTemplate.query(SQL_SEARCH, params, accountRowMapper);

    }

    @Override
    public Optional<Account> findByEmail(String email) {
        try {
            Account account = namedJdbcTemplate.queryForObject(SQL_FIND_BY_EMAIL, Collections.singletonMap("email", email), accountRowMapper);
            return Optional.of(account);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }

    }

    @Override
    public List<Account> findAll() {
        return namedJdbcTemplate.query(SQL_SELECT_ALL, accountRowMapper);
    }

    @Override
    public void save(Account entity) {
        Map<String, Object> params = new HashMap<>();
        params.put("firstName", entity.getFirstName());
        params.put("lastName", entity.getLastName());
        params.put("email", entity.getEmail());
        params.put("hashPassword", entity.getHashPassword());
        namedJdbcTemplate.update(SQL_INSERT_USER, params);
    }
}
