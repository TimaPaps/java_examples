package ru.ptv.site.repositories;

import java.util.List;

/**
 * 18.06.2021 - 20:13
 * 01_Java_Servlet_Application
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface CrudRepository<T, ID> {
    List<T> findAll();
    void save(T entity);
}
