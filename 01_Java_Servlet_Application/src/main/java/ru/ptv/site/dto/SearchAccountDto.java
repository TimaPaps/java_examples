package ru.ptv.site.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.ptv.site.models.Account;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 18.06.2021 - 19:03
 * 01_Java_Servlet_Application
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SearchAccountDto {
    private String firstName;
    private String lastName;

    // конвертация из модели
    public static SearchAccountDto from(Account account) {
        return SearchAccountDto.builder()
                .firstName(account.getFirstName())
                .lastName(account.getLastName())
                .build();
    }

    // конвертация в список
    public static List<SearchAccountDto> from(List<Account> accounts) {
        return accounts.stream().map(SearchAccountDto::from).collect(Collectors.toList());
    }
}
