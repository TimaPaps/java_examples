package ru.ptv.site.forms;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 20.06.2021 - 18:56
 * 01_Java_Servlet_Application
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SignUpForm {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
}
