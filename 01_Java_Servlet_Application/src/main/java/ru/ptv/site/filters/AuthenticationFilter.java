package ru.ptv.site.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * 21.06.2021 - 0:16
 * 01_Java_Servlet_Application
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@WebFilter("/*")
public class AuthenticationFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse)servletResponse;

        if (request.getRequestURI().contains("/users")) {
            // false - если ее не было - не нужно ее создавать
            HttpSession session = request.getSession(false);
            if (session != null) {
                if (session.getAttribute("authenticated") != null) {
                    filterChain.doFilter(request, response);
                } else {
                    System.out.println("Отсутствует атрибут");
                }
            } else {
                System.out.println("Отсутствует сессия");
            }
        } else {
            filterChain.doFilter(request, response);
        }

    }

    @Override
    public void destroy() {

    }
}
