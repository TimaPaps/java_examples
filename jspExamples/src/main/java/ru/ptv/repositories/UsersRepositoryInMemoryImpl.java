package ru.ptv.repositories;

import ru.ptv.fake.FakeStorage;
import ru.ptv.models.User;

import java.util.List;

/**
 * 07.06.2021 - 10:20
 * jspExamples
 *
 * Реализация объекта доступа к данным с испольованием фейкового хранилища.
 *
 * @author Timofey Papsuev
 * @version v1.0
 */
public class UsersRepositoryInMemoryImpl implements UsersRepository {
    public List<User> findAll() {
        return FakeStorage.storage().users();
    }

    @Override
    public void save(User user) {
        FakeStorage.storage().users.add(user);
    }

    @Override
    public boolean isExist(String userName, String password) {
        for (User user : FakeStorage.storage().users()) {
            if (user.getUserName().equals(userName) &&
                user.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }
}
