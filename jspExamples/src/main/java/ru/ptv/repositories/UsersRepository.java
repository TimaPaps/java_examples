package ru.ptv.repositories;

import ru.ptv.models.User;

import java.util.List;

/**
 * 07.06.2021 - 11:03
 * jspExamples
 *
 * @author Timofey Papsuev
 * @version v1.0
 */
public interface UsersRepository {
    List<User> findAll();
    void save(User user);
    boolean isExist(String userName, String password);
}
