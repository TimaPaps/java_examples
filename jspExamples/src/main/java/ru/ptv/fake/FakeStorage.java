package ru.ptv.fake;

import ru.ptv.models.User;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * 08.06.2021 - 7:28
 * jspExamples
 *
 * Класс, реализующий паттеррн Singleton
 * Представляет собой InMemory-хранилище для получения информации о зарегистрированных пользователях
 *
 * @author Timofey Papsuev
 * @version v1.0
 */
public class FakeStorage {
    // переменная, которая хранит ссылку на единственный экземпляр объекта класса FakeStorage
    private static final FakeStorage storage;
    // статический инициализатор, создающий объект класса FakeStorage. Вызывается один раз при загрузке класса в JVM
    static {
        storage = new FakeStorage();
    }
    // поле-список, хранящее список пользователей системы
    public List<User> users;
    // приватный констуктор, выполняющий инициализацию списка
    private FakeStorage() {
        this.users = new ArrayList<>();
        User user1 = new User("Tim", "123", LocalDate.parse("1982-11-17"));
        User user2 = new User("Tima", "1234", LocalDate.parse("1982-11-17"));
        User user3 = new User("Timofey", "12345", LocalDate.parse("1982-11-17"));
        users.add(user1);
        users.add(user2);
        users.add(user3);
    }
    // метод, предоставляющий доступ к объекту класса
    public static FakeStorage storage() {
        return storage;
    }
    // метод, возвращающий список пользователей
    public List<User> users() {
        return users;
    }
}
