package ru.ptv.sevlets;

import ru.ptv.models.User;
import ru.ptv.repositories.UsersRepository;
import ru.ptv.repositories.UsersRepositoryInMemoryImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

/**
 * 06.06.2021 - 21:36
 * jspExamples
 *
 * Сервлет для регистрации.
 *
 * @author Timofey Papsuev
 * @version v1.0
 */
@WebServlet("/signUp")
public class SignUpServlet extends HttpServlet {
    private UsersRepository usersRepository;

    @Override
    public void init() throws ServletException {
        this.usersRepository = new UsersRepositoryInMemoryImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<User> users = usersRepository.findAll();
        req.setAttribute("usersFromServer", users);
        RequestDispatcher dispatcher = req.getServletContext().getRequestDispatcher("/jsp/signUp.jsp");
        dispatcher.forward(req, resp);
    }

    //TODO: add BCrypt
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // вытащили данные регистрации
        String userName = req.getParameter("userName");
        String password = req.getParameter("password");
        LocalDate birthDate = LocalDate.parse(req.getParameter("birthDate"));
        // создали пользователя и сохранили его в хранилище
        User user = new User(userName, password, birthDate);
        usersRepository.save(user);
//        doGet(req, resp);
        resp.sendRedirect(req.getContextPath() + "/signUp");
    }
}
