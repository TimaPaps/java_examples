package ru.ptv.sevlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 08.06.2021 - 6:13
 * jspExamples
 *
 * Сервлет, который работает со страницей home.
 *
 * @author Timofey Papsuev
 * @version v1.0
 */
@WebServlet("/home")
public class HomeServlet extends HttpServlet {
    // в случае GET-запроса следует просто отдать страницу home
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getServletContext().getRequestDispatcher("/jsp/home.jsp").forward(req, resp);
    }

    // обработка запроса, который должен поменять цвет заголовка
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // получаем параметр запроса
        String color = req.getParameter("color");
        // создаем Cookie с данным значением
        // в Cookie нельзя хранить пароли, логины..., можно карзину, заказы и т.д. не важное, не личное...
        // куки и сессии необходимо переименовывать, для защиты от хакерских атак
        Cookie colorCookie = new Cookie("color", color);     //Cookie - ключ("color") -> значение(color)
        // кладем в ответ
        resp.addCookie(colorCookie);
        // перенаправляем пользователя обратно на страницу home
        resp.sendRedirect(req.getContextPath() + "/home");
    }
}
