package ru.ptv.sevlets;

import ru.ptv.repositories.UsersRepository;
import ru.ptv.repositories.UsersRepositoryInMemoryImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * 08.06.2021 - 7:15
 * jspExamples
 *
 * Сервлет, позволяющий пользователю войти в систему.
 *
 * @author Timofey Papsuev
 * @version v1.0
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    // ссылка на хранилище пользователей, один единственный список, т.к. Singleton
    private UsersRepository usersRepository;

    @Override
    public void init() throws ServletException {
        this.usersRepository = new UsersRepositoryInMemoryImpl();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getServletContext().getRequestDispatcher("/jsp/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // вытаскиваем из запроса имя пользователя и его пароль
        String userName = req.getParameter("userName");
        String password = req.getParameter("password");

        // если пользователь есть в системе
        if (usersRepository.isExist(userName, password)) {
            // создаем для него сессию на сервере
            HttpSession session = req.getSession();
            // кладем в атрибуты сессии атрибут user с именем пользователя
            // авторизовали пользователя
            // куки и сессии необходимо переименовывать, для защиты от хакерских атак
            session.setAttribute("user", userName);
            // перенаправляем на страницу home
            req.getServletContext().getRequestDispatcher("/home").forward(req, resp);
        } else {
            resp.sendRedirect(req.getContextPath() + "/login");
        }
    }
}
