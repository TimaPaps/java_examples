package ru.ptv.models;

import java.time.LocalDate;

/**
 * 07.06.2021 - 10:21
 * jspExamples
 *
 * @author Timofey Papsuev
 * @version v1.0
 */
public class User {
    private String userName;
    private String password;
    private LocalDate birthDate;

    public User(String userName, String password, LocalDate birthDate) {
        this.userName = userName;
        this.password = password;
        this.birthDate = birthDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }
}
