<%@ page import="ru.ptv.models.User" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: Timofey Papsuev
  Date: 07.06.2021
  Time: 11:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link href="/jspExamples-war/css/styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="form-style-2">
    <div class="form-style-2-heading">
        Please Sign Up!
    </div>
    <form method="post" action="/jspExamples-war/signUp">
        <label for="userName">User Name
            <input class="input-field" id="userName" type="text" name="userName">
        </label>
        <label for="birthDate">Birth Date
            <input class="input-field" id="birthDate" type="text" name="birthDate">
        </label>
        <label for="password">Password
            <input class="input-field" id="password" type="password" name="password">
        </label>
        <input type="submit" value="Sign Up">
    </form>
</div>
<div class="form-style-2">
    <div class="form-style-2-heading">
        Already registered!
    </div>
    <table>
        <tr>
            <th>User name</th>
            <th>Birth Date</th>
        </tr>
        <c:forEach items="${usersFromServer}" var="user">
            <tr>
                <td>${user.userName}</td>
                <td>${user.birthDate}</td>
            </tr>
        </c:forEach>

        <%--    <%--%>
        <%--        ArrayList<User> users =--%>
        <%--                (ArrayList) request.getAttribute("usersFromServer");--%>
        <%--    %>--%>

        <%--    <% for (User user : users) {--%>
        <%--    %>--%>
        <%--    <tr>--%>
        <%--        <td><%=user.getUserName()%>--%>
        <%--        </td>--%>
        <%--        <td><%=user.getBirthDate()%>--%>
        <%--        </td>--%>
        <%--    </tr>--%>
        <%--    <%}%>--%>
    </table>
</div>
</body>
</html>
