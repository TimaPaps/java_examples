<%--
  Created by IntelliJ IDEA.
  User: Timofey Papsuev
  Date: 08.06.2021
  Time: 7:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <link href="/jspExamples-war/css/styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="form-style-2">
    <div class="form-style-2-heading">
        Please Sign In!
    </div>
    <form method="post" action="/jspExamples-war/login">
        <label for="userName">User Name
            <input class="input-field" id="userName" type="text" name="userName">
        </label>
        <label for="password">Password
            <input class="input-field" id="password" type="password" name="password">
        </label>
        <input type="submit" value="Sign In">
    </form>
</div>
</body>
</html>
