package ru.ptv;

/**
 * 10.06.2021 - 9:04
 * springDemo
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class ByeMessage implements Message {

    private String text;

    public ByeMessage(String text) {
        this.text = "Bye " + text;
    }

    @Override
    public String getText() {
        return text;
    }
}
