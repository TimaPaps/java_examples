package ru.ptv;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 10.06.2021 - 10:11
 * springJavaConfigDemo
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Configuration
@ComponentScan(basePackages = "ru.ptv")
public class AppConfig {

    @Bean
    public MessageRenderer messageRenderer() {
        return new MessageRendererErrorImpl(message());
    }

    @Bean
    public Message message() {
        return new HelloMessage("Hello!");
    }
}
