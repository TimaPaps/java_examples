package ru.ptv;

/**
 * 10.06.2021 - 9:02
 * springDemo
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class HelloMessage implements Message {

    private String text;

    public HelloMessage(String text) {
        this.text = "Hello " + text;
    }

    @Override
    public String getText() {
        return text;
    }
}
