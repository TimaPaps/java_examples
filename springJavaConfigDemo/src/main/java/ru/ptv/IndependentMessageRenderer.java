package ru.ptv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 10.06.2021 - 9:39
 * springDemo
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Component
public class IndependentMessageRenderer {

    @Autowired
    private MessageRenderer renderer;

    public void print() {
        renderer.printMessage();
    }
}
