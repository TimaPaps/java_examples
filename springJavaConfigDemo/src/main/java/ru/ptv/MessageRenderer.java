package ru.ptv;

/**
 * 10.06.2021 - 9:24
 * springDemo
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface MessageRenderer {
    void printMessage();
}
