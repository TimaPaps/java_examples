create table fix_user
(
    id         bigserial primary key,
    first_name varchar(50),
    last_name  varchar(50)
);

insert into fix_user (first_name, last_name)
values ('Tima', 'Papsuev');
insert into fix_user (first_name, last_name)
values ('Vasya', 'Ivanov');
insert into fix_user (first_name, last_name)
values ('Oleg', 'Cidorov');

create table fix_car
(
    id       bigserial primary key,
    model    varchar(30),
    owner_id bigserial references fix_user (id)
);

insert into fix_car (model, owner_id)
values ('mers', 1);
insert into fix_car (model, owner_id)
values ('subaru', 1);
insert into fix_car (model, owner_id)
values ('bmw', 2);
insert into fix_car (model)
values ('opel');