package ru.ptv.mvc.dao;

import java.util.List;
import java.util.Optional;

/**
 * 08.06.2021 - 23:29
 * dbExample
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface CrudDao<T> {
    Optional<T> find(Long id);
    void save(T model);
    void update(T model);
    void delete(Long id);

    List<T> findAll();
}
