package ru.ptv.mvc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ptv.mvc.models.User;

import java.util.List;

/**
 * 11.06.2021 - 16:30
 * springMvc
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface UsersRepository extends JpaRepository<User, Long> {
    List<User> findAllByFirstName(String firstName);
}
