package ru.ptv.mvc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.ptv.mvc.models.Car;

import java.util.List;

/**
 * 11.06.2021 - 16:31
 * springMvc
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface CarsRepository extends JpaRepository<Car, Long> {
    List<Car> findAllByOwner_FirstName(String firstNameOwner);

    @Query(nativeQuery = true, value = "SELECT * FROM fix_car WHERE model = ?1;")
    List<Car> findAllByModel(String model);
}
