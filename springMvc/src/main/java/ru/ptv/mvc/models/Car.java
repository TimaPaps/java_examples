package ru.ptv.mvc.models;

import lombok.*;

import javax.persistence.*;

/**
 * 10.06.2021 - 14:45
 * springMvc
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString(exclude = "owner")
@Entity
@Table(name = "fix_car")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String model;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private User owner;
}
