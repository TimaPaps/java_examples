package ru.ptv.mvc.forms;

import lombok.Data;

/**
 * 11.06.2021 - 15:25
 * springMvc
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Data
public class UserForm {
    private String firstName;
    private String lastName;
}
