package ru.ptv.mvc.app;

import org.springframework.jdbc.datasource.DriverManagerDataSource;
import ru.ptv.mvc.dao.UsersDao;
import ru.ptv.mvc.dao.UsersDaoJdbcTemplateImpl;
import ru.ptv.mvc.models.User;

import java.util.List;

/**
 * 10.06.2021 - 15:16
 * springMvc
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/testdb");
        dataSource.setUsername("tima");
        dataSource.setPassword("");

        UsersDao usersDao = new UsersDaoJdbcTemplateImpl(dataSource);

        List<User> users = usersDao.findAll();
        System.out.println(users);
    }
}
