package ru.ptv.service.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ptv.service.models.Token;

import java.util.Optional;

/**
 * 17.06.2021 - 16:32
 * springRestDemo
 *
 * @author  Papsuev Timofey
 * @version v1.0
 */
public interface TokensRepository extends JpaRepository<Token, Long> {
    Optional<Token> findOneByValue(String value);
}
