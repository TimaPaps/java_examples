package ru.ptv.service.transfer;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.ptv.service.models.Token;

/**
 * 17.06.2021 - 16:18
 * springRestDemo
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Data
@AllArgsConstructor
public class TokenDto {

    private String value;

    public static TokenDto from(Token token) {
        return new TokenDto(token.getValue());
    }
}
