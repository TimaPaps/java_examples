package ru.ptv.service.models;

/**
 * 16.06.2021 - 6:19
 * springBootService
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public enum State {
    ACTIVE, BANNED, DELETED;
}
