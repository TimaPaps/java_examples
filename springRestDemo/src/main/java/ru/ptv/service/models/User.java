package ru.ptv.service.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.ptv.service.forms.UserForm;

import javax.persistence.*;
import java.util.List;

/**
 * 10.06.2021 - 14:41
 * springMvc
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "fix_user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;

    private String login;
    private String hashPassword;

    @Enumerated(value = EnumType.STRING)
    private Role role;
    @Enumerated(value = EnumType.STRING)
    private State state;

//    public static User from(UserForm form) {
//        return User.builder()
//                .firstName(form.getFirstName())
//                .lastName(form.getLastName())
//                .build();
//    }

    @OneToMany(mappedBy = "user")
    List<Token> tokens;
}
