package ru.ptv.service.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.ptv.service.forms.LoginForm;
import ru.ptv.service.services.LoginService;
import ru.ptv.service.transfer.TokenDto;

/**
 * 17.06.2021 - 15:41
 * springRestDemo
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping("/login")
    public ResponseEntity<TokenDto> login(@RequestBody LoginForm loginForm) {
        return ResponseEntity.ok(loginService.login(loginForm));
    }
}
