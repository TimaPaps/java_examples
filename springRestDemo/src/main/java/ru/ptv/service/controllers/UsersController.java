package ru.ptv.service.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.ptv.service.forms.UserForm;
import ru.ptv.service.models.User;
import ru.ptv.service.services.UsersService;
import ru.ptv.service.transfer.UserDto;

import java.util.List;

import static ru.ptv.service.transfer.UserDto.from;

/**
 * 17.06.2021 - 13:40
 * springRestDemo
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@RestController
public class UsersController {

    private final UsersService usersService;

    @Autowired
    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping("/users")
    public List<UserDto> getUsers() {
        return from(usersService.findAll());
    }

    @GetMapping("/users/{user-id}")
    public User getUser(@PathVariable("user-id") Long userId) {
        return usersService.findOne(userId);
    }

    @PostMapping("/users")
    public ResponseEntity<Object> addUser(@RequestBody UserForm userForm) {
        usersService.signUp(userForm);
        return ResponseEntity.ok().build();
    }
}
