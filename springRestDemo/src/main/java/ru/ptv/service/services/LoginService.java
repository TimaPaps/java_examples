package ru.ptv.service.services;

import ru.ptv.service.forms.LoginForm;
import ru.ptv.service.transfer.TokenDto;

/**
 * 17.06.2021 - 16:29
 * springRestDemo
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface LoginService {
    TokenDto login(LoginForm loginForm);
}
