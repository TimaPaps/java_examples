package ru.ptv.service.services;

import ru.ptv.service.forms.UserForm;
import ru.ptv.service.models.User;

import java.util.List;

/**
 * 16.06.2021 - 6:50
 * springBootService
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface UsersService {
    void signUp(UserForm userForm);
    List<User> findAll();
    User findOne(Long userId);
}
