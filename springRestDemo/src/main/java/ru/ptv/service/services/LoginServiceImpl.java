package ru.ptv.service.services;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.ptv.service.forms.LoginForm;
import ru.ptv.service.models.Token;
import ru.ptv.service.models.User;
import ru.ptv.service.repositories.TokensRepository;
import ru.ptv.service.repositories.UsersRepository;
import ru.ptv.service.transfer.TokenDto;

import java.util.Optional;

import static ru.ptv.service.transfer.TokenDto.from;

/**
 * 17.06.2021 - 16:31
 * springRestDemo
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
//@Component
@Service
public class LoginServiceImpl implements LoginService {

    private final TokensRepository tokensRepository;
    private final PasswordEncoder passwordEncoder;
    private final UsersRepository usersRepository;

    @Autowired
    public LoginServiceImpl(TokensRepository tokensRepository, PasswordEncoder passwordEncoder, UsersRepository usersRepository) {
        this.tokensRepository = tokensRepository;
        this.passwordEncoder = passwordEncoder;
        this.usersRepository = usersRepository;
    }

    @Override
    public TokenDto login(LoginForm loginForm) {
        Optional<User> userCandidate = usersRepository.findOneByLogin(loginForm.getLogin());

        if (userCandidate.isPresent()) {
            User user = userCandidate.get();

            if (passwordEncoder.matches(loginForm.getPassword(), user.getHashPassword())) {
                Token token = Token.builder()
                        .user(user)
                        .value(RandomStringUtils.random(10, true, true))
                        .build();

                tokensRepository.save(token);
                return from(token);
            }
        }
        throw new IllegalArgumentException("User Not Found");
    }
}

