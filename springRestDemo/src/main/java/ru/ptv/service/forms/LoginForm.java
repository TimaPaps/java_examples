package ru.ptv.service.forms;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 17.06.2021 - 15:47
 * springRestDemo
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginForm {
    private String login;
    private String password;
}
