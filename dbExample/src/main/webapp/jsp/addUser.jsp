<%--
  Created by IntelliJ IDEA.
  User: Timofey Papsuev
  Date: 08.06.2021
  Time: 7:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <link href="/dbExample_war/css/styles.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="form-style-2">
    <div class="form-style-2-heading">
        Please add User!
    </div>
    <form method="post" action="/dbExample_war/users">
        <label for="first-name">First Name
            <input class="input-field" id="first-name" type="text" name="first-name">
        </label>
        <label for="last-name">Last Name
            <input class="input-field" id="last-name" type="text" name="last-name">
        </label>
        <input type="submit" value="Add User">
    </form>
</div>
</body>
</html>
