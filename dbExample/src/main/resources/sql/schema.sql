create table test_user
(
    id         serial primary key,
    first_name varchar(50),
    last_name  varchar(50)
);

insert into test_user (first_name, last_name)
values ('Tima', 'Papsuev');
insert into test_user (first_name, last_name)
values ('Vasya', 'Ivanov');
insert into test_user (first_name, last_name)
values ('Oleg', 'Cidorov');

create table test_car
(
    id       serial primary key,
    model    varchar(30),
    owner_id integer references test_user (id)
);

insert into test_car (model, owner_id)
values ('mers', 1);
insert into test_car (model, owner_id)
values ('subaru', 1);
insert into test_car (model, owner_id)
values ('bmw', 2);
insert into test_car (model)
values ('opel');

select *
from test_user
         left join test_car on test_user.id = test_car.owner_id;

select *
from test_user
         right join test_car on test_user.id = test_car.owner_id;

select *
from test_user
         inner join test_car on test_user.id = test_car.owner_id;
