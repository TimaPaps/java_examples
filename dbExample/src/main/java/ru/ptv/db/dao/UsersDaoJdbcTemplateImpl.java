package ru.ptv.db.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import ru.ptv.db.models.Car;
import ru.ptv.db.models.User;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.*;

/**
 * 09.06.2021 - 11:18
 * dbExample
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class UsersDaoJdbcTemplateImpl implements UsersDao {

    private final JdbcTemplate template;

    private Map<Integer, User> usersMap = new HashMap<>();

    private static final String SQL_SELECT_ALL =
            "SELECT * FROM test_user";

    private static final String SQL_SELECT_ALL_BY_FIRST_NAME =
            "SELECT * FROM test_user WHERE first_name = ?";

//    private static final String SQL_SELECT_USER_WITH_CARS =
//            "SELECT * " +
//                    "FROM test_user " +
//                    "LEFT JOiN test_car ON test_user.id = test_car.owner_id " +
//                    "WHERE test_user.id = ?";

    private static final String SQL_SELECT_USER_WITH_CARS =
            "SELECT test_user.*, test_car.id as car_id, test_car.model " +
                    "FROM test_user " +
                    "LEFT JOIN test_car ON test_user.id = test_car.owner_id " +
                    "WHERE test_user.id = ?";

    public UsersDaoJdbcTemplateImpl(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }

    RowMapper<User> usersRowMapper = (resultSet, i) -> {
        return new User(
                resultSet.getInt("id"),
                resultSet.getString("first_name"),
                resultSet.getString("last_name"));
    };


    private final RowMapper<User> userRowMapper = (ResultSet resultSet, int i) -> {
        Integer id = resultSet.getInt("id");

        if (!usersMap.containsKey(id)) {
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");
            User user = new User(id, firstName, lastName, new ArrayList<>());
            usersMap.put(id, user);
        }

        Car car = new Car(resultSet.getInt("car_id"), resultSet.getString("model"), usersMap.get(id));

        usersMap.get(id).getCars().add(car);

        return usersMap.get(id);
    };

    @Override
    public Optional<User> find(Integer id) {
        template.query(SQL_SELECT_USER_WITH_CARS, userRowMapper, id);

        if (usersMap.containsKey(id)) {
            return Optional.of(usersMap.get(id));
        }
        return Optional.empty();
    }

    @Override
    public void save(User model) {

    }

    @Override
    public void update(User model) {

    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public List<User> findAll() {
        return template.query(SQL_SELECT_ALL, usersRowMapper);
    }

    @Override
    public List<User> findAllByFirstName(String firstName) {
        return template.query(SQL_SELECT_ALL_BY_FIRST_NAME, usersRowMapper, firstName);
    }
}
