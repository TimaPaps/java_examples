package ru.ptv.db.dao;

import ru.ptv.db.models.User;

import java.util.List;

/**
 * 08.06.2021 - 23:32
 * dbExample
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface UsersDao extends CrudDao<User> {
    List<User> findAllByFirstName(String firstName);
}
