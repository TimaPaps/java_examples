package ru.ptv.db.dao;

import java.util.List;
import java.util.Optional;

/**
 * 08.06.2021 - 23:29
 * dbExample
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface CrudDao<T> {
    Optional<T> find(Integer id);
    void save(T model);
    void update(T model);
    void delete(Integer id);

    List<T> findAll();
}
