package ru.ptv.db.sevlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

/**
 * 08.06.2021 - 16:27
 * dbExample
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
//@WebServlet("/users")
public class UsersServlet extends HttpServlet {
    private Connection connection;

    @Override
    public void init() throws ServletException {
        Properties properties = new Properties();

        try {
            properties.load(new FileInputStream(getServletContext().getRealPath("/WEB-INF/classes/db/db.properties")));
//            System.out.println(properties.getProperty("db.url"));
            String dbUrl = properties.getProperty("db.url");
            String dbUserName = properties.getProperty("db.userName");
            String dbPassword = properties.getProperty("db.password");
            String dbDriverClassName = properties.getProperty("db.driverClassName");

            Class.forName(dbDriverClassName);
            connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
        } catch (IOException | SQLException | ClassNotFoundException e) {
            throw new IllegalStateException(e);
        }
//        String dbUrl = "jdbc:postgresql://localhost:5432/testdb";
//        String dbUser = "tima";
//        String dbPassword = "";
//        String dbDriver = "org.postgresql.Driver";
//
//        try {
//            Class.forName(dbDriver);
//            connection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
//        } catch (SQLException | ClassNotFoundException e) {
//            throw new IllegalStateException(e);
//        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getServletContext().getRequestDispatcher("/jsp/addUser.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String firstName = req.getParameter("first-name");
        String lastName = req.getParameter("last-name");
        String BAD_SQL_INSERT = "INSERT INTO test_user (first_name, last_name) VALUES ('" + firstName + "', '" + lastName + "')";
        String GOOD_SQL_INSERT = "INSERT INTO test_user (first_name, last_name) VALUES (?, ?)";

        // очень
        try {
            Statement statement = connection.createStatement();
//            System.out.println(SQL_INSERT);

/* как нельзя делать!!!!!!!!!!!
//    sql иньекция - если ее вставить в поле имя, то удалится таблица:
//    temp','temp'); DROP TABLE test_car; SELECT ('temp
//    вот так будет выглядеть этот запрос к БД:
//    INSERT INTO test_user (first_name, last_name) VALUES ('temp','temp'); DROP TABLE test_car; SELECT ('temp', 'good')

            statement.execute(BAD_SQL_INSERT);
 */

            PreparedStatement preparedStatement = connection.prepareStatement(GOOD_SQL_INSERT);
            preparedStatement.setString(1, firstName);
            preparedStatement.setString(2, lastName);
            preparedStatement.execute();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
