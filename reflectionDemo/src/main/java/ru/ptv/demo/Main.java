package ru.ptv.demo;

import java.lang.reflect.Field;

/**
 * 10.06.2021 - 4:45
 * reflectionDemo
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {

        /* Рефлексия
            получать информацию о переменных, методах внутри класса, о самом классе, его конструкторах, реализованных интерфейсах и т.д.;
            получать новый экземпляр класса;
            получать доступ ко всем переменным и методам, в том числе приватным;
            преобразовывать классы одного типа в другой (cast);
            делать все это во время исполнения программы (динамически, в Runtime).
         */
        SomeClass someObject = new SomeClass();
        Class<SomeClass> someClassAsClass = (Class<SomeClass>) someObject.getClass();
        try {
            // экземпляр класса Field дает доступ к полям
            Field someField = someClassAsClass.getField("someField");
            System.out.println(someField.getType());

            Field[] fields = someClassAsClass.getFields();
            for (Field field : fields) {
                System.out.println(field.getType() + " " + field.getName());
            }

            System.out.println(someObject.someField);
            someField.set(someObject, 777);
            System.out.println(someObject.someField);

            Field privateField = someClassAsClass.getDeclaredField("somePrivate");
            System.out.println(privateField.getName());
//            // не можем изменить приватное поле
//            privateField.set(someObject, "Tima");
//            System.out.println(someObject.getSomePrivate());
            // можем изменить приватное поле
            privateField.setAccessible(true);
            privateField.set(someObject, "Tima");
            System.out.println(someObject.getSomePrivate());

            // экземпляр класса Method дает доступ к методам
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
