package ru.ptv.demo;

/**
 * 10.06.2021 - 4:43
 * reflectionDemo
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class SomeClass {
    public int someField;
    public String someStringField;
    private String somePrivate;

    public String getSomePrivate() {
        return somePrivate;
    }
}
