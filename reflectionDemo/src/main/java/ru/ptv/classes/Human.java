package ru.ptv.classes;

/**
 * 10.06.2021 - 5:18
 * reflectionDemo
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class Human {
    public int age;
    public String name;

    public Human() {
        this.age = 1;
        this.name = "default";
    }

    public Human(int age, String name) {
        this.age = age;
        this.name = name;
    }

    @Override
    public String toString() {
        return this.age + " " + this.name;
    }
}
