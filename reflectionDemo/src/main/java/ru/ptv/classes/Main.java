package ru.ptv.classes;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;

/**
 * 10.06.2021 - 5:20
 * reflectionDemo
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String className = scanner.next();
        Class aClass = null;
        Field[] fields;
        try {
            // по имени класса получаем доступ к полям класса
            aClass = Class.forName(className);

            fields = aClass.getFields();

            for (Field field : fields) {
                System.out.println(field.getType() + " " + field.getName());
            }
            // вводим ru.ptv.classes.Human

            // создаем объект.инстанс класса
            // newInstance() не создаст объект, если нет в классе пустого конструктора
            // т.е. newInstance() вызывает пустой конструктор для создания объектов public Human() {...}
            Object object = aClass.newInstance();
            System.out.println(object);
            // вводим ru.ptv.classes.Human

        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        try {
            aClass = Class.forName(className);

            fields = aClass.getFields();
            Class[] types = new Class[fields.length];

            for (int i = 0; i < types.length; i++) {
                types[i] = fields[i].getType();
            }

            Constructor constructor = aClass.getDeclaredConstructor(types);

            for (Class parameterType : constructor.getParameterTypes()) {
                System.out.println(parameterType.getName() + " ");
            }
            // вводим ru.ptv.classes.Human

            Integer intValue = 0;
            String stingValue = "";
            for (int i = 0; i < types.length; i++) {
                if (types[i].getName().equals("int")) {
                    intValue = scanner.nextInt();
                } else if (types[i].getName().equals("java.lang.String")) {
                    stingValue = scanner.next();
                }
            }
            // вводим возраст для человека
            // вводим имя для человека
            Object arguments[] = {intValue, stingValue};
            Object object = constructor.newInstance(arguments);
            System.out.println(object);

        } catch (NoSuchMethodException | ClassNotFoundException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        // вводим ru.ptv.classes.Human
        // вводим размер для стола
        // вводим название для стола
    }
}
