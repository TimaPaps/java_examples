package ru.ptv.classes;

/**
 * 10.06.2021 - 5:18
 * reflectionDemo
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class Table {
    public int size;
    public String color;

    public Table(int size, String color) {
        this.size = size;
        this.color = color;
    }

    @Override
    public String toString() {
        return "Table{" +
                "size=" + size +
                ", color='" + color + '\'' +
                '}';
    }
}
