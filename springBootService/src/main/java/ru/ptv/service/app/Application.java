package ru.ptv.service.app;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * 12.06.2021 - 1:03
 * springBootService
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@SpringBootApplication
@ComponentScan(basePackages = "ru.ptv.service")
@EnableJpaRepositories(basePackages = "ru.ptv.service.repositories")
@EntityScan(basePackages = "ru.ptv.service.models")
public class Application {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }
}
