package ru.ptv.service.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.ptv.service.forms.UserForm;
import ru.ptv.service.models.Role;
import ru.ptv.service.models.State;
import ru.ptv.service.models.User;
import ru.ptv.service.repositories.UsersRepository;

/**
 * 16.06.2021 - 6:51
 * springBootService
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Service
public class SignUpServiceImpl implements SignUpService {

    /*
    private final UsersRepository usersRepository;

    @Autowired
    public SignUpServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }
    */

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void signUp(UserForm userForm) {
        String hashPassword = passwordEncoder.encode(userForm.getPassword());

        User user = User.builder()
                .firstName(userForm.getFirstName())
                .lastName(userForm.getLastName())
                .login(userForm.getLogin())
                .hashPassword(hashPassword)
                .role(Role.USER)
                .state(State.ACTIVE)
                .build();

        usersRepository.save(user);
    }
}
