package ru.ptv.service.services;

import ru.ptv.service.forms.UserForm;

/**
 * 16.06.2021 - 6:50
 * springBootService
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface SignUpService {
    void signUp(UserForm userForm);
}
