package ru.ptv.service.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import ru.ptv.service.repositories.UsersRepository;

/**
 * 12.06.2021 - 1:05
 * springBootService
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Controller
public class UsersController {

//    // достаем my.property из application.properties
//    @Value("${my.property}")
//    private String myProperty;

    @Autowired
    private UsersRepository usersRepository;

    @GetMapping("/users")
    public String getUsersPage(ModelMap model) {
//        // достаем my.property из application.properties
//        System.out.println(myProperty);

        model.addAttribute("usersFromServer", usersRepository.findAll());
        return "users";
    }
}
