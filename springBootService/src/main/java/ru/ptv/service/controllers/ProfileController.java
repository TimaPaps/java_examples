package ru.ptv.service.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import ru.ptv.service.security.details.UserDetailsImpl;
import ru.ptv.service.transfer.UserDto;

import static ru.ptv.service.transfer.UserDto.from;

/**
 * 16.06.2021 - 23:54
 * springBootService
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
@Controller
public class ProfileController {

    @GetMapping("/")
    public String getProfilePage(ModelMap model, Authentication authentication) {

        if (authentication == null) {
            return "redirect:/login";
        }

        UserDetailsImpl details = (UserDetailsImpl)authentication.getPrincipal();
        UserDto user = from(details.getUser());
        model.addAttribute("user", user);
        return "profile";
    }
}
