package ru.ptv.service.models;

/**
 * 16.06.2021 - 6:17
 * springBootService
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public enum Role {
    ADMIN, USER;
}
