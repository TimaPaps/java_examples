package ru.ptv;

/**
 * 10.06.2021 - 7:18
 * springDemo
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class MessageRendererStandardImpl implements MessageRenderer {
    private Message message;

    public MessageRendererStandardImpl(Message message) {
        this.message = message;
    }

    public void printMessage() {
        System.out.println(message.getText());
    }
}
