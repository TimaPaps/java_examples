package ru.ptv;

/**
 * 10.06.2021 - 9:22
 * springDemo
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class MessageRendererErrorImpl implements MessageRenderer {
    private Message message;

    public MessageRendererErrorImpl(Message message) {
        this.message = message;
    }

    public void printMessage() {
        System.err.println(message.getText());
    }
}
