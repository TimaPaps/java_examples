package ru.ptv;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 10.06.2021 - 7:16
 * springDemo
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("ru.ptv/context.xml");

//        MessageRenderer renderer = (MessageRenderer) context.getBean("renderer");
//        renderer.printMessage();

        IndependentMessageRenderer renderer = context.getBean(IndependentMessageRenderer.class);
        renderer.print();
    }
}
