package ru.ptv;

/**
 * 10.06.2021 - 8:59
 * springDemo
 *
 * @author Papsuev Timofey
 * @version v1.0
 */
public interface Message {
    String getText();
}
